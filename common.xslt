﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xsd12d="http://www.12d.com/schema/xml12d-10.0">

  <!-- ### GENERAL REPORT DETAILS ### -->
  <xsl:template name="report_details">
    <xsl:param name="xml12d" />
    <xsl:param name="PROJECT_DETAILS" select="1"/>
    <xsl:if test="$PROJECT_DETAILS = 1">
      <table>
        <tbody>
          <tr>
            <td class="details_title">Project:</td>
            <td class="details_value">
              <xsl:value-of select="$xml12d/xsd12d:meta_data/xsd12d:application/xsd12d:project_name" />
            </td>
          </tr>
          <tr>
            <td class="details_title">Directory:</td>
            <td class="details_value">
              <xsl:value-of select="$xml12d/xsd12d:meta_data/xsd12d:application/xsd12d:project_folder" />
            </td>
          </tr>
          <tr>
            <td class="details_title">User:</td>
            <td class="details_value">
              <xsl:value-of select="$xml12d/xsd12d:meta_data/xsd12d:application/xsd12d:user" />
            </td>
          </tr>
          <tr>
            <td class="details_title">Created:</td>
            <td class="details_value">
              <xsl:value-of select="$xml12d/xsd12d:meta_data/xsd12d:application/xsd12d:export_date" />
            </td>
          </tr>
        </tbody>
      </table>
      <br />
      <br />
    </xsl:if>
  </xsl:template>

</xsl:stylesheet>
