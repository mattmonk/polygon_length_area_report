<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE stylesheet [
<!ENTITY tab 	"&#9;" >
<!ENTITY lf 	"&#10;" >
<!ENTITY cr 	"&#13;" >
<!ENTITY rarr   "&#x2192;" >
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xsd12d="http://www.12d.com/schema/xml12d-10.0">
	<xsl:output method="text"/>

	<!-- field delimiter -->
	<!-- The default delimiter is a tab despite this being for a comma-separated values filename
		 This is to ensure MS Excel can open the resulting file.
		 MS Excel does not correctly read/interpret CSV files in Unicode.
		 Changing to tab-delimited allows the file to be read in correctly by MS Excel -->
	<xsl:variable name="DELIM">
    <xsl:text>&tab;</xsl:text>
    <!-- <xsl:text>,</xsl:text> -->
	</xsl:variable>

	<xsl:strip-space elements="*" />

	<!-- #### GENERIC REPORT SETTINGS #### -->
	<!-- Include project details in header
		 1 = enabled, otherwise = disabled -->
	<xsl:variable name="PROJECT_DETAILS">1</xsl:variable>

	<!-- #### STYLING OF REPORTS #### -->
	<!-- Include Column Headings
		 1 = enabled, else = disabled -->
	<xsl:variable name="COLUMN_HEADINGS">1</xsl:variable>
	<!-- Uses UPPERCASE headings in the table output
		 Otherwise, headings will be Proper case.
		 0 = disabled, 1 = enabled -->
	<xsl:variable name="UPPER_CASE_HEADINGS">1</xsl:variable>


	<xsl:variable name="POLYGON_ELEMENT_SINGLE_COLUMN">0</xsl:variable>
	<xsl:variable name="SHOW_POLYGON_IDS">0</xsl:variable>
	<xsl:variable name="SHOW_POLYGON_TIMES">0</xsl:variable>
	<xsl:variable name="SHOW_POLYGON_AREA">1</xsl:variable>
	<xsl:variable name="SHOW_POLYGON_LENGTH">1</xsl:variable>
	<xsl:variable name="SHOW_POLYGON_TYPE">1</xsl:variable>
	<xsl:variable name="SHOW_POLYGON_CENTROID">1</xsl:variable>


	<!-- #### FORMATTING VARIABLES #### -->
	<!-- Formatting pattern for the format-number() function:
		0 = Digit
		# = Digit, zeros are not shown
		. = The position of the decimal point Example: ###.##
		, = The group separator for thousands. Example: ###,###.##
		% = Displays the number as a percentage. Example: ##%
		; = Pattern separator. The first pattern will be used for positive numbers and the second for negative numbers
	-->
	<!-- Generic formatting for 3 decimal places -->
	<xsl:variable name="format_3dp">0.000</xsl:variable>
	<!-- Formatting pattern for size -->
	<xsl:variable name="format_length">0.000</xsl:variable>
	<!-- Formatting pattern for area -->
	<xsl:variable name="format_area">0.0</xsl:variable>
	<!-- Decimal Format Definitions -->
	<!-- This currently simply shows non-numbers (NaN) as a blank.
		 Othewise, the output would show NaN -->
	<xsl:decimal-format NaN="" />	
	
	
	<xsl:template name="prettify_gmt_timestamp">
		<xsl:param name="timestamp" />
		<xsl:variable name="year" select="substring($timestamp, 1, 4)"/>
		<xsl:variable name="month" select="substring($timestamp, 6, 2)"/>
		<xsl:variable name="day" select="substring($timestamp, 9, 2)"/>
		<xsl:value-of select="$day"/>
		<xsl:text>/</xsl:text>
		<xsl:value-of select="$month"/>
		<xsl:text>/</xsl:text>
		<xsl:value-of select="$year"/>
		<xsl:text> </xsl:text>
		<xsl:value-of select="substring-before(substring-after($timestamp, 'T'), 'Z')" />
		<xsl:text> GMT</xsl:text>
	</xsl:template>
	
	<!-- ### GENERAL REPORT DETAILS TEMPLATE ### -->
	<xsl:template match="xsd12d:meta_data">
		<xsl:if test="$PROJECT_DETAILS = 1">
			<xsl:text>&lf;</xsl:text>
			<xsl:text>Project:</xsl:text>
			<xsl:value-of select="$DELIM" />
			<xsl:value-of select="xsd12d:application/xsd12d:project_name" />
			<xsl:text>&lf;</xsl:text>
			<xsl:text>Directory:</xsl:text>
			<xsl:value-of select="$DELIM" />
			<xsl:value-of select="xsd12d:application/xsd12d:project_folder" />
			<xsl:text>&lf;</xsl:text>
			<xsl:text>User:</xsl:text>
			<xsl:value-of select="$DELIM" />
			<xsl:value-of select="xsd12d:application/xsd12d:user" />
			<xsl:text>&lf;</xsl:text>
			<xsl:text>Created:</xsl:text>
			<xsl:value-of select="$DELIM" />
			<xsl:value-of select="xsd12d:application/xsd12d:export_date" />
			<xsl:text>&lf;</xsl:text>
		</xsl:if>
	</xsl:template>
	
	<xsl:template match="xsd12d:oplygon_length_areas">
		<xsl:apply-templates />
	</xsl:template>	
	
	<xsl:template match="xsd12d:polygon_length_area" >
		<xsl:apply-templates />
	</xsl:template>
		
	<xsl:template match="xsd12d:panel_settings">
		<xsl:text>&lf;</xsl:text>
		<xsl:text>Absolute areas:</xsl:text>
		<xsl:value-of select="$DELIM" />
		<xsl:value-of select="xsd12d:absolute_area" />
		<xsl:text>&lf;</xsl:text>
		<xsl:text>&lf;</xsl:text>
	</xsl:template>
	
	<xsl:template name="column_headings" >
		<xsl:choose>
			<xsl:when test="$POLYGON_ELEMENT_SINGLE_COLUMN = 1">
				<xsl:choose>
					<xsl:when test="$UPPER_CASE_HEADINGS = 1" >
						<xsl:text>POLYGON</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>Polygon</xsl:text>
					</xsl:otherwise>
				</xsl:choose>	
			</xsl:when>
			<xsl:otherwise>		
				<xsl:choose>
					<xsl:when test="$UPPER_CASE_HEADINGS = 1" >
						<xsl:text>NAME</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>Name</xsl:text>
					</xsl:otherwise>
				</xsl:choose>					
				<xsl:if test="$SHOW_POLYGON_IDS = 1">
					<xsl:value-of select="$DELIM" />
					<xsl:choose>
						<xsl:when test="$UPPER_CASE_HEADINGS = 1" >
							<xsl:text>ELEMENT ID</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>Element ID</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:value-of select="$DELIM" />
				</xsl:if>				
				
				<xsl:value-of select="$DELIM" />
				<xsl:choose>
					<xsl:when test="$UPPER_CASE_HEADINGS = 1" >
						<xsl:text>MODEL</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>Model</xsl:text>
					</xsl:otherwise>
				</xsl:choose>					
				<xsl:if test="$SHOW_POLYGON_IDS = 1">
					<xsl:value-of select="$DELIM" />
					<xsl:choose>
						<xsl:when test="$UPPER_CASE_HEADINGS = 1" >
							<xsl:text>MODEL ID</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>Model ID</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:if>	
				<xsl:if test="$SHOW_POLYGON_TIMES = 1">	
					<xsl:value-of select="$DELIM" />	
					<xsl:choose>
						<xsl:when test="$UPPER_CASE_HEADINGS = 1" >
							<xsl:text>TIME CREATED</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>Time Created</xsl:text>
						</xsl:otherwise>
					</xsl:choose>	
					<xsl:value-of select="$DELIM" />		
					<xsl:choose>
						<xsl:when test="$UPPER_CASE_HEADINGS = 1" >
							<xsl:text>TIME UPDATED</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>Time Updated</xsl:text>
						</xsl:otherwise>
					</xsl:choose>	
				</xsl:if>
			</xsl:otherwise>				
		</xsl:choose>
		<xsl:value-of select="$DELIM" />
		<xsl:if test="$SHOW_POLYGON_LENGTH = 1">		
			<xsl:choose>
				<xsl:when test="$UPPER_CASE_HEADINGS = 1" >
					<xsl:text>LENGTH</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Length</xsl:text>
				</xsl:otherwise>
			</xsl:choose>	
			<xsl:value-of select="$DELIM" />			
		</xsl:if>
		<xsl:if test="$SHOW_POLYGON_AREA = 1">	
			<xsl:choose>
				<xsl:when test="$UPPER_CASE_HEADINGS = 1" >
					<xsl:text>AREA</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Area</xsl:text>
				</xsl:otherwise>
			</xsl:choose>	
			<xsl:value-of select="$DELIM" />
		</xsl:if>		
		<xsl:if test="$SHOW_POLYGON_CENTROID = 1">	
			<xsl:choose>
				<xsl:when test="$UPPER_CASE_HEADINGS = 1" >
					<xsl:text>CENTROID X</xsl:text>
					<xsl:value-of select="$DELIM" />
					<xsl:text>CENTROID Y</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Centroid X</xsl:text>
					<xsl:value-of select="$DELIM" />
					<xsl:text>Centroid Y</xsl:text>
				</xsl:otherwise>
			</xsl:choose>	
			<xsl:value-of select="$DELIM" />
		</xsl:if>	
		<xsl:choose>
			<xsl:when test="$UPPER_CASE_HEADINGS = 1" >
				<xsl:text>CLOSED</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>Closed</xsl:text>
			</xsl:otherwise>
		</xsl:choose>		
		<xsl:value-of select="$DELIM" />
		<xsl:if test="$SHOW_POLYGON_TYPE = 1">	
			<xsl:choose>
				<xsl:when test="$UPPER_CASE_HEADINGS = 1" >
					<xsl:text>TYPE</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Type</xsl:text>
				</xsl:otherwise>
			</xsl:choose>	
		</xsl:if>
		<xsl:text>&lf;</xsl:text>
	</xsl:template>
	
	<xsl:template match="xsd12d:polygon_list">
		<xsl:call-template name="column_headings" />	
		<xsl:apply-templates />
		
		<xsl:call-template name="polygons_totals">
			<xsl:with-param name="polygons_node" select="." />
		</xsl:call-template>
	</xsl:template>	
	
	<xsl:template match="xsd12d:polygon">
	<!-- Call these in a specific order to match HTML column order -->
		<xsl:apply-templates select="xsd12d:object" />
		<xsl:value-of select="$DELIM" />
		<xsl:apply-templates select="xsd12d:length" />
		<xsl:value-of select="$DELIM" />
		<xsl:if test="xsd12d:area or xsd12d:area_absolute">
			<xsl:if test="$SHOW_POLYGON_AREA = 1">
				<xsl:choose>
					<xsl:when test="ancestor::xsd12d:panel_settings/xsd12d:absolute_area = 'true'">
						<xsl:value-of select="format-number(xsd12d:area_absolute, $format_area)" />
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="format-number(xsd12d:area, $format_area)" />
					</xsl:otherwise>
				</xsl:choose>
			</xsl:if>
		</xsl:if>
		<xsl:value-of select="$DELIM" />
		<xsl:choose>
			<xsl:when test="xsd12d:centroid">
				<xsl:apply-templates select="xsd12d:centroid" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$DELIM" />
			</xsl:otherwise>
		</xsl:choose>
		<xsl:value-of select="$DELIM" />
		<xsl:apply-templates select="xsd12d:closed" />
		<xsl:value-of select="$DELIM" />	
		<xsl:apply-templates select="xsd12d:type" />	
		<xsl:text>&lf;</xsl:text>
	</xsl:template>
		
	<xsl:template match="xsd12d:object">
		<xsl:choose>
			<xsl:when test="$POLYGON_ELEMENT_SINGLE_COLUMN = 1">
				<xsl:value-of select="xsd12d:model/xsd12d:name" />
				<xsl:text>&rarr;</xsl:text>
				<xsl:value-of select="xsd12d:element/xsd12d:name" />
			</xsl:when>
			<xsl:otherwise>		
				<xsl:value-of select="xsd12d:element/xsd12d:name" />
				<xsl:if test="$SHOW_POLYGON_IDS = 1">
					<xsl:value-of select="$DELIM" />
					<xsl:value-of select="xsd12d:element/xsd12d:id" />
				</xsl:if>			
				<xsl:value-of select="$DELIM" />			
				<xsl:value-of select="xsd12d:model/xsd12d:name" />
				<xsl:if test="$SHOW_POLYGON_IDS = 1">
					<xsl:value-of select="$DELIM" />
					<xsl:value-of select="xsd12d:model/xsd12d:id" />
				</xsl:if>
				<xsl:if test="$SHOW_POLYGON_TIMES = 1">
					<xsl:value-of select="$DELIM" />
					<xsl:call-template name="prettify_gmt_timestamp">
						<xsl:with-param name="timestamp" select="xsd12d:time_created" />
					</xsl:call-template>
					<xsl:value-of select="$DELIM" />
					<xsl:call-template name="prettify_gmt_timestamp">
						<xsl:with-param name="timestamp" select="xsd12d:time_updated" />
					</xsl:call-template>
				</xsl:if>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template match="xsd12d:length">
		<xsl:if test="$SHOW_POLYGON_LENGTH = 1">
			<xsl:value-of select="format-number(., $format_length)" />
		</xsl:if>
	</xsl:template>
			
	<xsl:template match="xsd12d:centroid">
		<xsl:if test="$SHOW_POLYGON_CENTROID = 1">
			<xsl:value-of select="format-number(xsd12d:x, $format_3dp)" />
			<xsl:value-of select="$DELIM" />
			<xsl:value-of select="format-number(xsd12d:y, $format_3dp)" />
		</xsl:if>
	</xsl:template>
	
	<xsl:template match="xsd12d:closed">
		<xsl:value-of select="." />
	</xsl:template>
	
	<xsl:template match="xsd12d:type">
		<xsl:if test="$SHOW_POLYGON_TYPE = 1">
			<xsl:value-of select="." />
		</xsl:if>
	</xsl:template>


	<!-- ### TOTALS ### -->
	<xsl:template name="polygons_totals">
		<xsl:param name="polygons_node" />
		
		<xsl:text>&lf;TOTALS&lf;&lf;</xsl:text>
		<xsl:choose>
		  <xsl:when test="$UPPER_CASE_HEADINGS = 1">
		  <xsl:text>POLYGONS</xsl:text>
		</xsl:when>
		<xsl:otherwise>
		  <xsl:text>Polygons</xsl:text>
		</xsl:otherwise>
		</xsl:choose>
		<xsl:value-of select="$DELIM" />
		<xsl:choose>
		  <xsl:when test="$UPPER_CASE_HEADINGS = 1">
		  <xsl:text>TOTAL LENGTH</xsl:text>
		</xsl:when>
		<xsl:otherwise>
		  <xsl:text>Total Length</xsl:text>
		</xsl:otherwise>
		</xsl:choose>
		<xsl:value-of select="$DELIM" />
		<xsl:choose>
		  <xsl:when test="$UPPER_CASE_HEADINGS = 1">
		  <xsl:text>TOTAL AREA</xsl:text>
		</xsl:when>
		<xsl:otherwise>
		  <xsl:text>Total Area</xsl:text>
		</xsl:otherwise>
		</xsl:choose>
		<xsl:text>&lf;</xsl:text>
		<xsl:value-of select="count($polygons_node/xsd12d:polygon)" />
		<xsl:value-of select="$DELIM" />
		<xsl:value-of select="format-number(sum($polygons_node/xsd12d:polygon/xsd12d:length[number(.) = number(.)]), $format_length)" />
		<xsl:value-of select="$DELIM" />
		<xsl:choose>
			<xsl:when test="ancestor::xsd12d:polygon_length_area/xsd12d:panel_settings/xsd12d:absolute_area = 'true'">
				<xsl:value-of select="format-number(sum($polygons_node/xsd12d:polygon/xsd12d:area_absolute[number(.) = number(.)]), $format_length)" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="format-number(sum($polygons_node/xsd12d:polygon/xsd12d:area[number(.) = number(.)]), $format_length)" />
			</xsl:otherwise>
		</xsl:choose>
		<xsl:text>&lf;</xsl:text>
	</xsl:template>


	
		
		
	<!-- ### MAIN TEMPLATE ### -->
	<xsl:template match="/">
		<xsl:for-each select="xsd12d:xml12d">
			<xsl:text>12d Model&lf;</xsl:text>
			<xsl:text>Polygons Length &amp; Area Report</xsl:text>
			<xsl:text>&lf;</xsl:text>
			<xsl:apply-templates />
		</xsl:for-each>
	</xsl:template>

</xsl:stylesheet>	