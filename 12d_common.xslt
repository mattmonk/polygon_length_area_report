﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xsd12d="http://www.12d.com/schema/xml12d-10.0">

  <xsl:template name="dashes">
    <xsl:param name="count" select="1"/>
    <xsl:if test="$count > 0">
      <xsl:text>-</xsl:text>
      <xsl:call-template name="dashes">
        <xsl:with-param name="count" select="$count - 1"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

  <xsl:template name="zeros">
    <xsl:param name="count" select="1"/>
    <xsl:if test="$count > 0">
      <xsl:text>0</xsl:text>
      <xsl:call-template name="zeros">
        <xsl:with-param name="count" select="$count - 1"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

  <xsl:template name="spaces">
    <xsl:param name="count" select="1"/>
    <xsl:if test="$count > 0">
      <xsl:text> </xsl:text>
      <xsl:call-template name="spaces">
        <xsl:with-param name="count" select="$count - 1"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

  <xsl:template name="print_text_left_justify">
    <xsl:param name="value"/>
    <xsl:param name="width" select="14"/>
    <xsl:value-of select="$value"/>
    <xsl:call-template name="spaces">
      <xsl:with-param name="count" select="$width - string-length($value)"/>
    </xsl:call-template>
  </xsl:template>

  <xsl:template name="print_text_right_justify">
    <xsl:param name="value"/>
    <xsl:param name="width" select="14"/>
    <xsl:call-template name="spaces">
      <xsl:with-param name="count" select="$width - string-length($value)"/>
    </xsl:call-template>
    <xsl:value-of select="$value"/>
  </xsl:template>

  <!-- Center justify text -->
  <xsl:template name="print_text_center_justify">
    <xsl:param name="value"/>
    <xsl:param name="width" select="14"/>
    <xsl:call-template name="spaces">
      <xsl:with-param name="count" select="floor(($width - string-length($value)) div 2)"/>
    </xsl:call-template>
    <xsl:value-of select="$value"/>
    <xsl:call-template name="spaces">
      <xsl:with-param name="count" select="ceiling(($width - string-length($value)) div 2)"/>
    </xsl:call-template>
  </xsl:template>

  <!-- not yet debugged and not working -->
  
  <xsl:template name="print_text">
    <xsl:param name="value"/>
    <xsl:param name="width" select="14"/>
    <xsl:param name="justify" select="left"/>

    <xsl:for-each select="$justify">
      <xsl:choose>
        <xsl:when test="name()=&quot;left&quot;">

          <xsl:value-of select="$value"/>
          <xsl:call-template name="spaces">
            <xsl:with-param name="count" select="$width - string-length($value)"/>
          </xsl:call-template>

        </xsl:when>
        <xsl:when test="name()=&quot;right&quot;">

          <xsl:call-template name="spaces">
            <xsl:with-param name="count" select="$width - string-length($value)"/>
          </xsl:call-template>
          <xsl:value-of select="$value"/>

        </xsl:when>
        <xsl:when test="name()=&quot;centre&quot;">

          <xsl:call-template name="spaces">
            <xsl:with-param name="count" select="$width - string-length($value)"/>
          </xsl:call-template>
          <xsl:value-of select="$value"/>

        </xsl:when>
      </xsl:choose>
    </xsl:for-each>
    
  </xsl:template>

  <xsl:template name="print_integer">
    <xsl:param name="value"/>
    <xsl:param name="width" select="8"/>
    <xsl:variable name="format" select="0"/>
    <xsl:variable name="result" select="format-number(number($value), $format)"/>
    <xsl:call-template name="spaces">
      <xsl:with-param name="count" select="$width - string-length($result)"/>
    </xsl:call-template>
    <xsl:value-of select="$result"/>
  </xsl:template>

  <xsl:template name="print_real">
    <xsl:param name="value"/>
    <xsl:param name="width" select="14"/>
    <xsl:param name="precision" select="3"/>
    <xsl:choose>
      <xsl:when test="$value != 'null'">
        <xsl:variable name="formatzeros">
          <xsl:call-template name="zeros">
            <xsl:with-param name="count" select="$precision"/>
          </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="format" select="concat('0.',$formatzeros)"/>
        <xsl:choose>
          <xsl:when test="string-length($value)">
            <xsl:variable name="result" select="format-number(number($value), $format)"/>
            <xsl:call-template name="spaces">
              <xsl:with-param name="count" select="$width - string-length($result)"/>
            </xsl:call-template>
            <xsl:value-of select="$result"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:call-template name="spaces">
              <xsl:with-param name="count" select="$width"/>
            </xsl:call-template>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>  
      <xsl:otherwise>
        <xsl:call-template name="print_text_right_justify">
          <xsl:with-param name="value" select="' '"/>
          <xsl:with-param name="width" select="$width"/>
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
    <!--
    <xsl:variable name="result" select="format-number(number($value), $format)"/>
    <xsl:call-template name="spaces">
      <xsl:with-param name="count" select="$width - string-length($result)"/>
    </xsl:call-template>
    <xsl:value-of select="$result"/>
    -->
  </xsl:template>

  <xsl:template name="print_real_14.4">
    <xsl:param name="value"/>
    <xsl:call-template name="print_real">
      <xsl:with-param name="value" select="$value"/>
      <xsl:with-param name="width" select="14"/>
      <xsl:with-param name="precision" select="4"/>
    </xsl:call-template>
  </xsl:template>

  <xsl:template name="print_model_name_string_name">
    <xsl:param name="value"/>
    <xsl:for-each select="xsd12d:model/xsd12d:name">
      <xsl:value-of select="string(.)"/>
    </xsl:for-each>
    <xsl:text>-></xsl:text>
    <xsl:for-each select="xsd12d:element/xsd12d:name">
      <xsl:value-of select="string(.)"/>
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="print_model_name_string_name_quoted">
    <xsl:param name="value"/>
    <xsl:text>"</xsl:text>
    <xsl:for-each select="xsd12d:model/xsd12d:name">
      <xsl:value-of select="string(.)"/>
    </xsl:for-each>
    <xsl:text>-></xsl:text>
    <xsl:for-each select="xsd12d:element/xsd12d:name">
      <xsl:value-of select="string(.)"/>
    </xsl:for-each>
    <xsl:text>"</xsl:text>
  </xsl:template>

  <xsl:template name="print_dms_angle">
    <xsl:param name="degree"/>
    <xsl:param name="minute"/>
    <xsl:param name="second"/>
    <xsl:for-each select="$degree">
      <xsl:value-of select="format-number(number(string(.)), '##0')"/>
    </xsl:for-each>
    <xsl:text>°</xsl:text>
    <xsl:for-each select="$minute">
      <xsl:value-of select="format-number(number(string(.)), '00')"/>
    </xsl:for-each>
    <xsl:text>'</xsl:text>
    <xsl:for-each select="$second">
      <xsl:value-of select="format-number(number(string(.)), '00.00')"/>
    </xsl:for-each>
    <xsl:text>"</xsl:text>
  </xsl:template>

  <!-- Graeme replaced this with below
  <xsl:template name="print_dms">
    <xsl:param name="angle"/>
    <xsl:for-each select="$angle">
      <xsl:call-template name="print_dms_angle">
        <xsl:with-param name="degree" select="$angle/xsd12d:dms/xsd12d:degree"/>
        <xsl:with-param name="minute" select="$angle/xsd12d:dms/xsd12d:minute"/>
        <xsl:with-param name="second" select="$angle/xsd12d:dms/xsd12d:second"/>
      </xsl:call-template>
    </xsl:for-each>
  </xsl:template>
  -->
  
  <!--added Ben Pham-->
  <!--from here-->

  <!-- Graeme replaced this with below
  <xsl:template name="print_dms_bearing">
    <xsl:param name="degree"/>
    <xsl:param name="minute"/>
    <xsl:param name="second"/>
    <xsl:value-of select="format-number(number(string($degree)), '##0')"/>
    <xsl:text>°</xsl:text>
    <xsl:value-of select="format-number(number(string($minute)), '00')"/>
    <xsl:text>'</xsl:text>
    <xsl:value-of select="format-number(number(string($second)), '00.00')"/>
    <xsl:text>"</xsl:text>
  </xsl:template>

  <xsl:template name="angle_to_bearing">
    <xsl:param name="angle"/>
    <xsl:for-each select="$angle">
      <xsl:call-template name="print_dms_bearing">
        <xsl:with-param name="degree" select="(449 - $angle/xsd12d:dms/xsd12d:degree) mod 360"/>
        <xsl:with-param name="minute" select="59  - $angle/xsd12d:dms/xsd12d:minute"/>
        <xsl:with-param name="second" select="60  - $angle/xsd12d:dms/xsd12d:second"/>
      </xsl:call-template>
    </xsl:for-each>
  </xsl:template>
  -->
  
  <!--to here-->

  <!--added Graeme Winfield-->
  <!--from here-->

  <xsl:template name="print_dms_bearing">
    <xsl:param name="degree"/>
    <xsl:param name="minute"/>
    <xsl:param name="second"/>
    <xsl:value-of select="format-number(number(string($degree)), '##0')"/>
    <xsl:if test="$degree > -1">
      <xsl:text>°</xsl:text>
    </xsl:if>
    <xsl:value-of select="format-number(number(string($minute)), '00')"/>
    <xsl:if test="$degree > -1">
      <xsl:text>'</xsl:text>
    </xsl:if>
    <xsl:value-of select="format-number(number(string($second)), '00.00')"/>
    <xsl:if test="$degree > -1">
      <xsl:text>"</xsl:text>
    </xsl:if>
  </xsl:template>

  <xsl:template name="print_dms">
    <xsl:param name="angle"/>
    <xsl:choose>
      <!-- seconds = 60 -->
      <xsl:when test="format-number(number(string($angle/xsd12d:dms/xsd12d:second)), '00.00') = 60">
        <xsl:choose>
          <!-- minutes = 60 -->
          <xsl:when test="$angle/xsd12d:dms/xsd12d:minute + 1 = 60">
            <xsl:call-template name="print_dms_bearing">
              <xsl:with-param name="degree" select="$angle/xsd12d:dms/xsd12d:degree + 1"/>
              <xsl:with-param name="minute" select="0"/>
              <xsl:with-param name="second" select="0"/>
            </xsl:call-template>
          </xsl:when>
          <!-- minutes not = 60 -->
          <xsl:otherwise>
            <xsl:call-template name="print_dms_bearing">
              <xsl:with-param name="degree" select="$angle/xsd12d:dms/xsd12d:degree"/>
              <xsl:with-param name="minute" select="$angle/xsd12d:dms/xsd12d:minute + 1"/>
              <xsl:with-param name="second" select="0"/>
            </xsl:call-template>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <!-- seconds not = 60 -->
      <xsl:otherwise>
        <xsl:choose>
          <!-- minutes = 60 -->
          <xsl:when test="$angle/xsd12d:dms/xsd12d:minute = 60">
            <xsl:call-template name="print_dms_bearing">
              <xsl:with-param name="degree" select="$angle/xsd12d:dms/xsd12d:degree + 1"/>
              <xsl:with-param name="minute" select="0"/>
              <xsl:with-param name="second" select="$angle/xsd12d:dms/xsd12d:second"/>
            </xsl:call-template>
          </xsl:when>
          <!-- minutes not = 60 -->
          <xsl:otherwise>
            <xsl:call-template name="print_dms_bearing">
              <xsl:with-param name="degree" select="$angle/xsd12d:dms/xsd12d:degree"/>
              <xsl:with-param name="minute" select="$angle/xsd12d:dms/xsd12d:minute"/>
              <xsl:with-param name="second" select="$angle/xsd12d:dms/xsd12d:second"/>
            </xsl:call-template>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="separate_dms_and_round">
    <xsl:param name="BinDecDeg"/>
    <xsl:variable name="degrees" select="floor($BinDecDeg)"/>
    <xsl:variable name="minutes_seconds" select="($BinDecDeg - $degrees) * 60"/>
    <xsl:variable name="minutes" select="floor($minutes_seconds)"/>
    <xsl:variable name="seconds" select="($minutes_seconds - $minutes) * 60"/> 
    <xsl:choose>
      <!-- seconds = 60 -->
      <xsl:when test="format-number(number(string($seconds)), '00.00') = 60">
        <xsl:choose>
          <!-- minutes = 60 -->
          <xsl:when test="$minutes + 1 = 60">
            <xsl:call-template name="print_dms_bearing">
              <xsl:with-param name="degree" select="$degrees + 1"/>
              <xsl:with-param name="minute" select="0"/>
              <xsl:with-param name="second" select="0"/>
            </xsl:call-template>
          </xsl:when>
          <!-- minutes not = 60 -->
          <xsl:otherwise>
            <xsl:call-template name="print_dms_bearing">
              <xsl:with-param name="degree" select="$degrees"/>
              <xsl:with-param name="minute" select="$minutes + 1"/>
              <xsl:with-param name="second" select="0"/>
            </xsl:call-template>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <!-- seconds not = 60 -->
      <xsl:otherwise>
        <xsl:choose>
          <!-- minutes = 60 -->
          <xsl:when test="$minutes = 60">
            <xsl:call-template name="print_dms_bearing">
              <xsl:with-param name="degree" select="$degrees + 1"/>
              <xsl:with-param name="minute" select="0"/>
              <xsl:with-param name="second" select="$seconds"/>
            </xsl:call-template>
          </xsl:when>
          <!-- minutes not = 60 -->
          <xsl:otherwise>
            <xsl:call-template name="print_dms_bearing">
              <xsl:with-param name="degree" select="$degrees"/>
              <xsl:with-param name="minute" select="$minutes"/>
              <xsl:with-param name="second" select="$seconds"/>
            </xsl:call-template>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="angle_to_bearing">
    <xsl:param name="angle"/>
    <xsl:for-each select="$angle">
      <xsl:call-template name="separate_dms_and_round">
        <xsl:with-param name="BinDecDeg" select="(450 - $angle/xsd12d:decimal_degree) mod 360"/>
      </xsl:call-template>
    </xsl:for-each>
  </xsl:template>
  
  <!--to here-->

</xsl:stylesheet>
